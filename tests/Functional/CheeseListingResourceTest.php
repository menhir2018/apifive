<?php


namespace App\Tests\Functional;


use App\Entity\CheeseListing;
use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class CheeseListingResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    public function testCreateCheeseListing()
    {
        $client = self::createClient();
        $client->request('POST', '/api/cheeses', [
            'json' => []
        ]);
        $this->assertResponseStatusCodeSame(401);

        $cheesyData = [
            "title"=> "Nuovo formaggio",
            "price"=> 500,
            "description"=> "super eccellente queso"
        ];
        $client->request('POST', '/api/cheeses',[
            'json'=> $cheesyData
        ]);
        $this->assertResponseStatusCodeSame(401);

        $authenticateUser = $this->createUserAndLogIn($client, 'pinco2.pallo@gigio.it','foo');
        $otherUser = $this->createUser( 'pinco2@gigio.it','foo');

        $client->request('POST', '/api/cheeses',[
            'json'=> []
        ]);
        $this->assertResponseStatusCodeSame(400);



        $client->request('POST', '/api/cheeses',[
            'json'=> $cheesyData
        ]);
        $this->assertResponseStatusCodeSame(201);


        $client->request('POST', '/api/cheeses',[
            'json'=> $cheesyData + ['owner'=> '/api/users/'.$otherUser->getId()]
        ]);
        $this->assertResponseStatusCodeSame(400, 'not passing the correct owner');


        $client->request('POST', '/api/cheeses',[
            'json'=> $cheesyData + ['owner'=> '/api/users/'.$authenticateUser->getId()]
        ]);
        $this->assertResponseStatusCodeSame(201);

    }

    public function testUpdateCheeseListing()
    {
        $client = self::createClient();
        $user1 = $this->createUser('user1@gnail.com', 'foo');
        $user2 = $this->createUser('user2@gnail.com', 'foo');

        $cheeseListing = new CheeseListing();
        $cheeseListing->setTitle('cheddar');
        $cheeseListing->setDescription('descccccc');
        $cheeseListing->setPrice(70000);
        $cheeseListing->setOwner($user1);
        $cheeseListing->setIsPublished(true);


        $em = $this->getEntityManager();
        $em->persist($cheeseListing);
        $em->flush();


        $this->logIn($client, 'user2@gnail.com', 'foo' );

        $client->request('PUT', '/api/cheeses/'.$cheeseListing->getId(),
            [
                'json' =>[
                    'title' => 'Updated',
                    'owner' =>  '/api/users/'.$user2->getId()
                ]
            ]);
        $this->assertResponseStatusCodeSame('403');

        $this->logIn($client, 'user1@gnail.com', 'foo' );

        $client->request('PUT', '/api/cheeses/'.$cheeseListing->getId(),
            [
                'json' =>[
                    'title' => 'Updated'
                ]
            ]);
        $this->assertResponseStatusCodeSame('200');
    }

    public function testGetCheeseListingCollection()
    {
        $client = self::createClient();

        $user = $this->createUser('pppiii@ggg.it', 'zoo');

        $cheeseListing1 = new CheeseListing();
        $cheeseListing1->setTitle('cheese1');
        $cheeseListing1->setDescription('super cheese ....');
        $cheeseListing1->setPrice(1000);
        $cheeseListing1->setOwner($user);
        $cheeseListing1->setIsPublished(false);

        $cheeseListing2 = new CheeseListing();
        $cheeseListing2->setTitle('cheese2');
        $cheeseListing2->setDescription('super cheese ....');
        $cheeseListing2->setPrice(1000);
        $cheeseListing2->setOwner($user);
        $cheeseListing2->setIsPublished(true);

        $cheeseListing3 = new CheeseListing();
        $cheeseListing3->setTitle('cheese3');
        $cheeseListing3->setDescription('super cheese ....');
        $cheeseListing3->setPrice(1000);
        $cheeseListing3->setOwner($user);
        $cheeseListing3->setIsPublished(true);

        $em = $this->getEntityManager();

        $em->persist($cheeseListing1);
        $em->persist($cheeseListing2);
        $em->persist($cheeseListing3);

        $em->flush();

        $client->request('GET', '/api/cheeses');
        $this->assertJsonContains(['hydra:totalItems' => 2]);
    }


    public function testGetCheeseListingItemIsPublished()
    {
        $client = self::createClient();

        $user = $this->createUserAndLogIn($client,'pppiii@ggg.it', 'zoo');

        $cheeseListing1 = new CheeseListing();
        $cheeseListing1->setTitle('cheese1');
        $cheeseListing1->setDescription('super cheese ....');
        $cheeseListing1->setPrice(1000);
        $cheeseListing1->setOwner($user);
        $cheeseListing1->setIsPublished(false);

        $em = $this->getEntityManager();

        $em->persist($cheeseListing1);


        $em->flush();

        $client->request('GET', '/api/cheeses/'.$cheeseListing1->getId());
        $this->assertResponseStatusCodeSame('404');


        $client->request('GET', '/api/users/'.$user->getId());
        $data = $client->getResponse()->toArray();
        $this->assertEmpty($data['cheeseListings']);
    }
}