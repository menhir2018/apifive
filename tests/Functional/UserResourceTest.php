<?php


namespace App\Tests\Functional;


use App\Entity\User;
use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class UserResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;

    public function testCreateUSer()
    {
        $client  = self::createClient();

        $client->request('POST', '/api/users', [
            'json'=>[
                'email' => 'pincopallo@gnail.com',
                'username' => 'pincopallo',
                'password' => 'foo'
            ]
        ]);

        $this->assertResponseStatusCodeSame('201');

        $this->logIn($client, 'pincopallo@gnail.com', 'foo' );
        
    }

    public function testUpdateUser()
    {
        $client  = self::createClient();
        $user = $this->createUserAndLogIn($client, 'pincopallo@gnail.com', 'foo');

        $client->request('PUT', '/api/users/'.$user->getId(),[
            'json' => [
                'username' => 'nuovo',
                'roles' => ['ROLE_ADMIN']
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'username' => 'nuovo'
        ]);

        $em = $this->getEntityManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($user->getId());

        $this->assertEquals(['ROLE_USER'], $user->getRoles());

    }

    public function testGetUser()
    {
        $client = self::createClient();

        $user = $this->createUser( 'pincopallo2@gnail.com', 'foo');
        $this->createUserAndLogIn( $client, 'pinco3@gnail.com', 'foo');

        $user->setPhoneNumber('88558558');
        $em = $this->getEntityManager();
        $em->flush();

        $client->request('GET', '/api/users/'.$user->getId());
        $this->assertJsonContains([
            'username' => 'pincopallo2'
        ]);

        $data = $client->getResponse()->toArray();
        $this->assertArrayNotHasKey('phoneNumber', $data);


        // refresh the user and elevate to Admin
        $user = $em->getRepository(User::class)->find($user->getId());
        $user->setRoles(['ROLE_ADMIN']);
        $em->flush();

        $this->logIn($client, 'pincopallo2@gnail.com', 'foo');

        $client->request('GET', '/api/users/'.$user->getId());
        $this->assertJsonContains([
            'phoneNumber' => '88558558'
        ]);

    }

}